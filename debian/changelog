alt-ergo (2.4.2-2) unstable; urgency=medium

  * Team upload.
  * Re-enable the patch for non-native architectures.

 -- Julien Puydt <jpuydt@debian.org>  Sat, 06 Aug 2022 23:04:05 +0200

alt-ergo (2.4.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Drop all patches (upstreamed).

 -- Julien Puydt <jpuydt@debian.org>  Sat, 06 Aug 2022 16:55:11 +0200

alt-ergo (2.4.1-4) unstable; urgency=medium

  * Add conditional patch for non-native architectures. (Closes: #1013362)

 -- Julien Puydt <jpuydt@debian.org>  Mon, 04 Jul 2022 09:30:03 +0200

alt-ergo (2.4.1-3) unstable; urgency=medium

  * Support cmdliner 1.1.1 using upstream patch.

 -- Julien Puydt <jpuydt@debian.org>  Wed, 29 Jun 2022 11:31:51 +0200

alt-ergo (2.4.1-2) unstable; urgency=medium

  * Team upload.
  * Ship the examples (Closes: #1011966).

 -- Julien Puydt <jpuydt@debian.org>  Sat, 28 May 2022 08:36:08 +0200

alt-ergo (2.4.1-1) unstable; urgency=medium

  * Team upload.
  * Fix d/watch.
  * New upstream release.
  * Adapt to new upstream build system.
  * Drop patches (obsolete).
  * Complete b-deps.
  * Add a patch to work with a recent menhir.
  * Update d/copyright.
  * Adapt to new upstream install system.

 -- Julien Puydt <jpuydt@debian.org>  Fri, 22 Apr 2022 11:07:37 +0200

alt-ergo (2.0.0-8) unstable; urgency=low

  [ Debian Janitor ]
  * debian/copyright: use spaces rather than tabs to start continuation lines.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ralf Treinen ]
  * Standards-Version 4.6.0 (no change)
  * Drop "Suggests: why" which is no longer in debian (closes: #977737)
  * New homepage at OCamlPRO

 -- Ralf Treinen <treinen@debian.org>  Wed, 27 Oct 2021 21:28:46 +0200

alt-ergo (2.0.0-7) unstable; urgency=medium

  * Team upload
  * Add empty override_dh_dwz to prevent FTBFS
  * Update debian/watch
  * Bump debhelper compat level to 13
  * Add Rules-Requires-Root: no

 -- Stéphane Glondu <glondu@debian.org>  Tue, 11 Aug 2020 10:05:10 +0200

alt-ergo (2.0.0-6) unstable; urgency=medium

  * Build without graphical interface, due to the removal of gtksourceview2:
    - drop build-dependencies liblablgtk2-gnome-ocaml-dev and
      liblablgtksourceview2-ocaml-dev
    - drop building and installing the gui from debian/rules
    - drop mention of the gui from the package description
    - announce this in debian/NEWS
    - drop override of dh_auto_{build,install} which are no longer needed
      since we do not have to specify gui any more
  * Drop override of dh_dwz which is no longer needed.
  * Standards-Version 4.5.0 (no change)

 -- Ralf Treinen <treinen@debian.org>  Wed, 15 Apr 2020 09:56:28 +0200

alt-ergo (2.0.0-5) unstable; urgency=medium

  * Add libnum-ocaml-dev to Build-Depends

 -- Stéphane Glondu <glondu@debian.org>  Sun, 01 Sep 2019 08:18:21 +0200

alt-ergo (2.0.0-4) unstable; urgency=high

  * Team upload
  * Fix compilation with camlzip >= 1.08

 -- Stéphane Glondu <glondu@debian.org>  Wed, 31 Jul 2019 11:08:36 +0200

alt-ergo (2.0.0-3) unstable; urgency=medium

  * Install files directly without going through dh_install
  * d/rules:
    - drop setting of BUILD_DATE which is no longer useful
    - drop inclusion of ocamlvars.mk which is no longer useful

 -- Ralf Treinen <treinen@debian.org>  Thu, 14 Feb 2019 04:21:22 +0100

alt-ergo (2.0.0-2) unstable; urgency=medium

  * Install plugins only on architectures with dynamic linking

 -- Ralf Treinen <treinen@debian.org>  Wed, 13 Feb 2019 22:40:56 +0100

alt-ergo (2.0.0-1) unstable; urgency=medium

  * Adjust debian/watch to the new *-free releases
  * New upstream release
    - refreshed patch 0001-dont-activate-debug-flags
    - dropped patch 0002-non-free-dropped which is no longer needed as upstream
      now publishes a completely free version
    - dropped patch 0003-allow-set-build-date since upstream has replaced
      the build-date by a release-date.
    - dropped patch spelling which has been applied upstream
    - dropped patch ocplib-simplex-0.4 since issue solved by upstream
  * debian/copyright:
    - license changed from CeCILL-C to Apache 2.0
    - https in format header
  * Build-dependencies:
    - ocplib-simplex: put a version constraint as indicated by upstream
    - ocaml-nox: bump minimal version to 4.04 as indicated by upstream
    - added menhir
    - debian-compat (=12)
  * Re-activate building and installing of the gui.
  * Do not install fpa-theory-2017-01-04-16h00.why which seems cruft
  * Override dh_dwz
  * Standards-version 4.3.0 (no change)

 -- Ralf Treinen <treinen@debian.org>  Wed, 13 Feb 2019 21:02:29 +0100

alt-ergo (1.30+dfsg1-2) unstable; urgency=medium

  * Since lablgtk2 does no longer build liblablgtksourceview2-ocaml-dev:
    - d/rules: stop building gui
    - drop build-dependencies on liblablgtk2-gnome-ocaml-dev and
      liblablgtksourceview2-ocaml-dev
    - drop mention of gui from the package description
    - added d/NEWS entry about this
  * Vcs-{Browser,Git}: switch to salsa
  * d/watch: minor fix
  * Standards-Version 4.2.1 (no change)

 -- Ralf Treinen <treinen@debian.org>  Tue, 13 Nov 2018 20:43:44 +0100

alt-ergo (1.30+dfsg1-1) unstable; urgency=medium

  * Repack source to exclude the non-free/ directory (closes: #883755).
  * Patch ocplib-simplex-0.4: patch to compile with version 0.4 of
    ocplib-simplex-ocaml-dev (closes: #882162).
  * Standards-Version 4.1.2 (no change)
  * debian/watch: drop pgpsigurlmangle

 -- Ralf Treinen <treinen@debian.org>  Thu, 21 Dec 2017 20:01:21 +0100

alt-ergo (1.30-1) unstable; urgency=medium

  * new upstream version:
    - refreshed patch 0001-dont-activate-debug-flags
    - refreshed patch 0002-non-free-dropped
    - refreshed patch 0003-allow-set-build-date
    - added build-dependency on ocplib-simplex-ocaml-dev, libzip-ocaml-dev,
      and ocaml-findlib
  * debhelper compatibility level 10:
    - bumped build-dependency on debhelper
    - bumped value in debian/compat
    - debian/rules: sequence before options in dh invocation
  * standards-version 3.9.8 (no change)
  * debian/control: dropped redundant Testsuite
  * updated Vcs-{Git,Browser}
  * patch spelling: various upstream spelling errors

 -- Ralf Treinen <treinen@debian.org>  Mon, 12 Dec 2016 19:58:57 +0100

alt-ergo (1.01-1) unstable; urgency=medium

  * new upstream version. Refreshed patches.
  * debian/watch: version 4. Add option pgpsigurlmangle.
  * standards-version 3.9.7 (no change)
  * bump version of build-dependency ocaml, according to upstream install
    instructions.

 -- Ralf Treinen <treinen@debian.org>  Tue, 01 Mar 2016 21:17:13 +0100

alt-ergo (0.99.1+dfsg1-4) unstable; urgency=low

  * debian/patches/series: add missing newline
  * debian/control: XS-Testsuite => Testsuite
  * debian/control: drop duplicated Section
  * debian/control: canonical URI in VCS fields
  * debian/copyright: Makefile.in => Makefile.configurable.in

 -- Ralf Treinen <treinen@debian.org>  Tue, 26 May 2015 21:56:56 +0200

alt-ergo (0.99.1+dfsg1-3) unstable; urgency=medium

  * debian/control: fix domain name in Vcs fields
  * explicitly set the build date to the date of the last changelog entry
    (closes: #786846):
    - add patch 0003-allow-set-build-date to set the build date in the
      Makefile in a variable
    - debian/rules: pass the correct value of the build date to the Makefile
    Thanks to Juan Picca <jumapico@gmail.com> and Jakub Wilk <jwilk@debian.org>
    for the patch!
  * rename patch 0001-dont-activate-debug-flag to be consistent with other
    patches

 -- Ralf Treinen <treinen@debian.org>  Tue, 26 May 2015 20:48:35 +0200

alt-ergo (0.99.1+dfsg1-2) unstable; urgency=low

  * upload to unstable.

 -- Ralf Treinen <treinen@debian.org>  Mon, 04 May 2015 19:05:42 +0200

alt-ergo (0.99.1+dfsg1-1) experimental; urgency=medium

  * New upstream version.
  * debian/copyright: add Files-Excluded: non-free
  * debian/watch: mangle suffix +dfsg\d*
  * debian/rules:
    - override for dh_auto_clean target: call "make clean" only
      when config.status present
    - override for dh_auto_install: drop install-pack target
  * update debian patch 0001-No-need-to-activate-debug-flag which now applies
    to the file Makefile.users
  * new patch 0002-non-free-dropped: adapt Makefile.users to the removal of
    the non-free directory.
  * Drop the libalt-ergo-ocaml-dev binary package since upstream does no
    longer support installation of development libraries.
  * Standards-Version 3.9.6 (no change)
  * Added DEP8-style package tests:
    - debian/control: add XS-Testsuite field
    - added debian/tests, with two test scripts on the examples installed with
      the package.

 -- Ralf Treinen <treinen@debian.org>  Wed, 31 Dec 2014 13:02:03 +0100

alt-ergo (0.95.2-3) sid; urgency=medium

  * Remove previously introduced patch since relevant API change
    has been reverted in OCamlgraph 1.8.5.
    - Remove 0002-Port-to-OCamlgraph-1.8.4.patch
    - Build-Depend on OCamlgraph >= 1.8.5~.

 -- Mehdi Dogguy <mehdi@debian.org>  Sun, 27 Apr 2014 21:42:15 +0200

alt-ergo (0.95.2-2) sid; urgency=medium

  * Port to OCamlgraph 1.8.4 (Closes: #743072)
    - add 0002-Port-to-OCamlgraph-1.8.4.patch

 -- Mehdi Dogguy <mehdi@debian.org>  Sun, 30 Mar 2014 23:07:47 +0200

alt-ergo (0.95.2-1) sid; urgency=low

  * New upstream release.
  * Update debian/watch, now points to ocamlpro site.
  * Refresh patch 0001-No-need-to-activate-debug-flag.patch
  * Drop patch 0002-Do-not-run-the-test-if-test.mlw-is-absent-and-use-be.patch:
    the file test.mlw is no longer relevant.
  * drop patches that have been applied by upstream:
    - 0003-Fix-a-typo.patch
    - 0004-Add-rules-and-targets-for-gui.byte.patch
    - 0005-Look-for-cma-instead-of-cmxa-for-lablgtksourceview2.patch
    - 0007-Fix-all-target.patch
    - 0008-Split-install-pack-into-two-separate-targets-opt-and.patch
    - 0009-clean-remove-META.patch
  * Add build-dependency on libzarith-ocaml-dev
  * debian/rules:
    - drop backup of .depend which is no longer needed
    - drop overwrite for dh_auto_configure
    - in dh_auto_build target, drop touching of configure
  * debian/copyright:
    - update Download field to ocamlpro
    - Upstream Contact : add alt-ergo-bugs mailing list
    - Update copyright holder and year for files *
    - Put paragraphs into the right order
  * install examples/ into /usr/share/doc/alt-ergo
  * Standards-version 3.9.5 (no change)

 -- Ralf Treinen <treinen@debian.org>  Sun, 17 Nov 2013 14:25:38 +0100

alt-ergo (0.95.1-3) unstable; urgency=low

  * make libalt-ergo-ocaml-dev conflict and replace with alt-ergo (<<
    0.95.1-1) to resolve file conflict with versions of the package before
    the split into two binary packages (closes: #718010).

 -- Ralf Treinen <treinen@debian.org>  Sat, 24 Aug 2013 10:56:21 +0200

alt-ergo (0.95.1-2) unstable; urgency=low

  * upload to unstable

 -- Ralf Treinen <treinen@debian.org>  Fri, 10 May 2013 21:19:12 +0200

alt-ergo (0.95.1-1) experimental; urgency=low

  * New upstream release.
  * Split off a package for the development libraries.
  * Patches:
    - Adapted all patches to new upstream.
    - Merge patch 0009-fix-install-gui-target into
      0004-Add-rules-and-targets-for-gui.byte
    - Add patch 0009-clean-remove-META.patch
    - Drop patch 0006-PRECM-O-X-is-needed-to-link-altgr-ergo.patch as
      upstream's makefile is doing the right thing.
  * debian/rules:
     - dh_auto-build: don't call "make altgr-ergo.$(OCAML_BEST)" since this
       is now subsumed by "make all"
     - drop the dh_install override since upstream's Makefile does the
       right thing
     - configure target: remove the "alt_ergo" from the libdir flag
       as this is added in upstream's Makefile.
     - Drop creation of the META file by debian/rules as this is now done by
       upstream's Makefile.
  * Drop debian/alt-ergo.dirs.in as all needed directories are created by
    upstream's Makefile.
  * Source-format 3.0 (quilt):
    - debian/source/format: update
    - debian/rules: drop "quilt" option from dh invocation
    - debian/control: drop quilt from build-dependencies
    - drop debian/README.source which is no longer necessary
  * Drop build-dependencies on autotools-dev, autoconf which are not needed.
    Touch configure in debian/rules to assure it is newer than configure.in.
  * Remove debian/docs as the upstream README file is useless for users of
    the debian package.
  * update clean target:
    - invoke dist-clean instead of clean in debian/rules
    - drop debian/clean which is no longer necessary (with patch
      0009-clean-remove-META.patch applied)
  * Rewrite long package description.
  * debian/watch: add extra directory per release.
  * debian/copyright:
    - machine-readable format 1.0
    - update copyright years to 2006-2011
    - add copyright holder Francois Bobot
  * Added myself as uploader.
  * Standards-Version 3.9.4 (no change)

 -- Ralf Treinen <treinen@debian.org>  Wed, 17 Apr 2013 09:45:39 +0200

alt-ergo (0.94-2) unstable; urgency=high

  * Add autoconf to Build-Depends to resolve an FTBFS (Closes: #669539).
  * Setting urgency to "high" to fix the RC bug.

 -- Mehdi Dogguy <mehdi@debian.org>  Mon, 14 May 2012 14:11:48 +0200

alt-ergo (0.94-1) unstable; urgency=low

  * New upstream release.
  * Rebase patches

 -- Mehdi Dogguy <mehdi@debian.org>  Thu, 08 Dec 2011 17:18:57 +0100

alt-ergo (0.93-3) unstable; urgency=low

  * Fix lablgtksourceview2 detection on bytecode architectures.
    - 0005-Look-for-cma-instead-of-cmxa-for-lablgtksourceview2.patch

 -- Mehdi Dogguy <mehdi@debian.org>  Sat, 16 Apr 2011 16:27:24 +0200

alt-ergo (0.93-2) unstable; urgency=low

  * Add patch to fix FTBFS on bytecode architectures.
    - 0004-Add-rules-and-targets-for-gui.byte.patch

 -- Mehdi Dogguy <mehdi@debian.org>  Sat, 16 Apr 2011 00:47:04 +0200

alt-ergo (0.93-1) unstable; urgency=low

  * New upstream release.
  * Update copyright file.
  * Add liblablgtk2-gnome-ocaml-dev and liblablgtksourceview2-ocaml-dev
    to Build-Depends (needed by altgr-ergo).

 -- Mehdi Dogguy <mehdi@debian.org>  Fri, 15 Apr 2011 23:31:11 +0200

alt-ergo (0.91-2) unstable; urgency=low

  * Fix build on bytecode architecures: "test" target should depend on
    "best" alternative, and not "opt".

 -- Mehdi Dogguy <mehdi@debian.org>  Tue, 01 Jun 2010 22:25:38 +0200

alt-ergo (0.91-1) unstable; urgency=low

  * New upstream release.
  * Update my email address and remove DMUA
  * Refresh packaging (dh-ocaml, dh overrides, quilt)
    - Do not run tests when input files are absent
      0002-Do-not-run-the-test-if-test.mlw-is-absent.patch
  * Bump standards to 3.8.4
    - Add a debian/README.source
  * Add a debian/source/format (keep 1.0 format, for now)
  * Fix a typo in the sources
    - Add 0003-Fix-a-typo.patch

 -- Mehdi Dogguy <mehdi@debian.org>  Tue, 01 Jun 2010 21:24:28 +0200

alt-ergo (0.9-1) unstable; urgency=low

  * New Upstream Version
  * Update build dependencies to ease OCaml 3.11.1 transition
  * Bump standards version to 3.8.2

 -- Mehdi Dogguy <dogguy@pps.jussieu.fr>  Mon, 06 Jul 2009 23:16:02 +0200

alt-ergo (0.8-1) unstable; urgency=low

  * Initial release (Closes: #468557)

 -- Mehdi Dogguy <dogguy@pps.jussieu.fr>  Tue, 24 Feb 2009 10:15:46 +0100
